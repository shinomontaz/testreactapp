@ECHO OFF

:: Assign all Path variables
SET _JAVA_OPTIONS=-Xmx2512M
SET JAVA_HOME=D:\opt\Android Studio\jre
SET ANDROID_HOME=D:\opt\Android SDK

gradlew assembleRelease --stacktrace

PAUSE
